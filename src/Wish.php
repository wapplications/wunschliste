<?php

class Wish extends \WUL\DataObject {
    static $filename = WISHES_FILE;
    static protected $cache = [];
    //protected $code, $title, $color;
    
    static function get_vars() {
        return [
            'id'=>'int', 
            'title'=>'string', 
            'isbn'=>['string', ''], 
            'author'=>['string', ''], 
            'url'=>['string', ''], 
            'marked'=>['bool', false]
        ];
    }
    

}
