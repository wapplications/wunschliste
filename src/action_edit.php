<?php
$id = $REQUEST->consumePath();

if (!$id) {
    echo template('template.site.php',[
        'body'=> '<a href="'. editUrl('new').'">+ Neuer Wunsch</a> | <a href="'.listUrl().'">Übersicht</a>'.\WUL\Renderer\renderComponent('a:WishList', [], Wish::get_cache()),
    ]);
} else {
    $error = false;

    if ($id === 'new') {
        $id = 0;
        $wish = new Wish((object)['title'=>'','author'=>'', 'isbn'=>'', 'url'=>'', 'marked'=>false], false);
    } else {
        $id = intval($id);
        try {
            $wish = Wish::get_item($id);
        } catch (\Exception $e) {
            die('error: wish');
        }
    }
    

    if ($REQUEST->pHas('save-wish')) {
        $data = (object)$REQUEST->pExtract(Wish::get_vars());
        
        $data->id = $id;
        if (!$id) {
            $wish = new Wish($data); // in den cache mit aufnehmen
        } else {
            $wish->fromObject($data); // bestehendes updaten
        }
        if (empty($wish->title)) {
            $error = true;
        }
        //o($wish);
        if (!$error) {
            Wish::save();
            \WUL\redirect(editUrl());
        }
    } else if ($REQUEST->pHas('save-wish-delete')) {
        if ($id && $wish && $wish->id) {
            $wish->delete();
            \WUL\redirect(editUrl());
        }
    }


    echo template('template.site.php',[
        'body'=> \WUL\Renderer\renderComponent('a:WishForm', ['error'=>$error], $wish),
    ]);
}
