<?php

if (!defined('ENTRY_POINT')) die();

require_once 'weppo_ul/wul.php';

define('INTERN', 1);

\WUL\Config::setConfig(include 'config.php');

\WUL\TemplateCall::setDir('templates/');


define ('HTTP_ROOT', '/'); // <?= HTTP_ROOT ? >

global $REQUEST;
global $USER;

$REQUEST = new \WUL\Request($_SERVER['REQUEST_URI'], $_GET, $_POST);




$lists = [];
$edits = [];
foreach (WUL\Config::getConfig()->users as &$user) {
    $lists[$user->list] = $user;
    $edits[$user->edit] = $user;
}


/*
/list/xxxxxx
/edit/yyyyyy
/edit/yyyyyy/2
 */

$action = $REQUEST->consumePath();
$code = $REQUEST->consumePath();
if ($action === 'list') {
    $USER = $lists[$code] ?? null;
} else if ($action === 'edit') {
    $USER = $edits[$code] ?? null;
} else {
    die('bad request (1)');
}
if (!$USER) {
    die('bad request (2)');
}





define('LIST_NAME', $USER->list);
define('EDIT_NAME', $USER->edit);
define('WISHES_FILE', ROOT.'/data/wishes.'.LIST_NAME.'.json');

require_once 'src/Wish.php';


$a = [
    'title'=>'BücherWunsch',
];
\WUL\TemplateCall::pushData($a);



function editUrl($t='') {
    return HTTP_ROOT . 'edit/' . EDIT_NAME . '/' . $t;
}
function listUrl($t='') {
    return HTTP_ROOT . 'list/' . LIST_NAME . '/' . $t;
}

Wish::load();
