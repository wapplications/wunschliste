<?php

namespace bs {
    function Card(\WUL\Renderer\Props &$props, &$contents) {
        $props->set('class', $props->get('class').' card');
        return \WUL\Renderer\renderComponent('div', $props, $contents);
    }
    
    function CardBody(\WUL\Renderer\Props &$props, &$contents) {
        $props->set('class', $props->get('class').' card-body');
        return \WUL\Renderer\renderComponent('div', $props, $contents);
    }
    
    function CardHeader(\WUL\Renderer\Props &$props, &$contents) {
        $props->set('class', $props->get('class').' card-header');
        return \WUL\Renderer\renderComponent('div', $props, $contents);
    }
    
    function CardFooter(\WUL\Renderer\Props &$props, &$contents) {
        $props->set('class', $props->get('class').' card-footer');
        return \WUL\Renderer\renderComponent('div', $props, $contents);
    }

    function FormInput(\WUL\Renderer\Props &$props, &$contents) {
        $sublabel = $props->get('sublabel');
        return \WUL\Renderer\renderComponent('div', ['class'=>'form-group row'], [
            ['label', ['class'=>'col-md-2 text-right'], [\WUL\Renderer\text($props->get('label'))      ]],
            ['div', ['class'=>'col-md-10'], [
                ['input', ['class'=>'form-control', 'value'=>$props->get('value'), 'name'=>$props->get('name')], []],
                ($sublabel ? ('<small class="text-muted">'.$sublabel.'</small>') : '')
            ]],
        ]);
    }
    
    function FormCheck(\WUL\Renderer\Props &$props, &$contents) {
        return \WUL\Renderer\renderComponent('div', ['class'=>'form-group row'], [
            ['div', ['class'=>'col-md-10 offset-md-2'], [
                ['label', [], [
                    ['input', ['class'=>'', 'value'=>'1', 'type'=>'checkbox', 'name'=>$props->get('name'), 'checked'=>$props->get('value')], []],
                    \WUL\Renderer\text($props->get('label'))
                ]],
            ]],
        ]);
    }
    
}



namespace c {
    
    
    // align-self-start  align-self-end
    function Wish(\WUL\Renderer\Props &$props, &$c) {
        $url = $c->url;
        if (!$url) {
            if ($c->isbn) {
                $url = 'https://www.buch7.de/store/simple_search_results?search='. urlencode($c->isbn);
                $urlText = 'Link zu Buch7';
            }
        } else {
            $urlText = $c->url;
        }
        return \WUL\Renderer\renderComponent('bs:Card', ['class'=>'wish mt-4' . ($c->marked?' marked':''), 'data-id'=>$c->id], [
            
            ['bs:CardHeader', [], [
                ['div', ['class'=>'row align-items-start'], [
                    ['h3', ['class'=>'col '], [\WUL\Renderer\text($c->title)]],
                    ['h5', ['class'=>'text-muted col  text-right'], [\WUL\Renderer\text($c->author)]],
                ]]
            ]],
            
            ['bs:CardBody', [], [
                $c->isbn ? 'ISBN / EAN:' : '',
                $c->isbn ? ['code', [], [\WUL\Renderer\text($c->isbn)]] : '',
                $url ? 'Web:' : '',
                $url ? ['a', ['href'=>$url,'target'=>'_blank'], [\WUL\Renderer\text($urlText)]] : '',
            ]],
            
            ['bs:CardFooter', [], [
                ['label', [], [['input', ['onclick'=>'submit_marked.bind(this)()',  'type'=>'checkbox', 'checked'=>$c->marked], []], ' Jemand kümmert sich']],
            ]],
            
        ]);
    }

    function WishList(\WUL\Renderer\Props &$props, &$contents) {
        $items = [];
        foreach ($contents as &$c) {
            $items[] = ['c:Wish', [], $c];
        }
        return \WUL\Renderer\renderComponent('div', ['class'=>'wishlist'], $items);
    }

} // namespace c




namespace a {
    
    function FormSubmit(\WUL\Renderer\Props &$props, &$contents) {
        return \WUL\Renderer\renderComponent('div', ['class'=>'form-group row'], [
            ['div', ['class'=>'col-md-10 offset-md-2'], [
                ['input', ['class'=>'btn btn-primary', 'value'=>$props->get('label'), 'type'=>'submit', 'name'=>$props->get('name')], []],
                $props->get('delete') ? ['input', ['onclick'=>'return confirm("Wirklich löschen?");','class'=>'btn btn-secondary', 'value'=>'Löschen', 'type'=>'submit', 'name'=>$props->get('name').'-delete'], []] : '',
                ['a', ['class'=>'btn btn-link','href'=>editUrl()], ['Abbrechen']],
            ]],
        ]);
    }
    
    function WishForm(\WUL\Renderer\Props &$props, &$c) {
        return \WUL\Renderer\renderComponent('form', ['method'=>'post','class'=>''], [
            $props->get('error') ? '<p class="text-danger">Bitte Angaben überprüfen.</p>' : '',
            ['bs:FormInput',  ['label'=>'Titel',      'name'=>'title', 'value'=>$c->title], []],
            ['bs:FormInput',  ['label'=>'Autor',      'name'=>'author', 'value'=>$c->author], []],
            ['bs:FormInput',  ['label'=>'ISBN / EAN', 'name'=>'isbn', 'value'=>$c->isbn], []],
            ['bs:FormInput',  ['label'=>'URL', 'sublabel'=>'leer für ISBN-Suche auf buch7.de', 'name'=>'url', 'value'=>$c->url], []],
            ['bs:FormCheck',  ['label'=>'Markiert',   'name'=>'marked', 'value'=>$c->marked], []],
            ['a:FormSubmit',  ['label'=>'Speichern',  'name'=>'save-wish', 'delete'=>!!$c->id], []],
        ]);
    }
    
    function Wish(\WUL\Renderer\Props &$props, &$c) {
        return \WUL\Renderer\renderComponent('a', ['href'=>editUrl($c->id),'class'=>'list-group-item wish' . ($c->marked?' marked':''), 'data-id'=>$c->id], [\WUL\Renderer\text($c->title)]);
    }

    function WishList(\WUL\Renderer\Props &$props, &$contents) {
        $items = [];
        foreach ($contents as &$c) {
            $items[] = ['a:Wish', [], $c];
        }
        return \WUL\Renderer\renderComponent('div', ['class'=>'list-group wishlist admin'], $items);
    }

} // namespace a

