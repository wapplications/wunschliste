<?php

$command = $REQUEST->consumePath();

//o(Wish::get_var_names());
/*
$g = ['a'=>'123', 'b'=>'ertzui', 'c'=>'false', 'd'=>'true', 'e'=>'1'];
$r = new WUL\Request('/', $g, $g);
o($r->gExtract(['a', 'b', 'c', 'd', 'e', 'f']));
o($r->gExtract(['a'=>'int', 'b'=>'string', 'c'=>'bool', 'd'=>'bool', 'e'=>'bool', 'f'=>'string', 'g'=>['string', '###'], 'h'=>['int', 42], 'i'=>['bool', true]]));
die();
*/

if ($command === 'mark') {
    $id = $REQUEST->consumePath();
    if (!is_numeric($id)) $id = null;
    $id = intval($id);
    if (!$id) {
        die('error:id');
    }
    try {
        $wish = Wish::get_item($id);
    } catch (\Exception $e) {
        die('error: wish');
    }
    $marked = $_POST['marked'] ?? 'false';
    $marked = $marked === 'true';
    $wish->marked = $marked;
    Wish::save();
    echo 'ok';
    return;
}

echo template('template.site.php',[
    'body'=> \WUL\Renderer\renderComponent('c:WishList', [], Wish::get_cache()),
]);

