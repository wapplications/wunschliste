<?php
if (!defined('INTERN')) die();
global $USER;
?>
<!doctype html>
<html>
    <head>
        <title><?= get('title', true) ?></title>
        <meta name="viewport" content="width=device-width"/>
        
        <script src="<?= HTTP_ROOT ?>lib/jquery-3.3.1.min.js" ></script>
        <script src="<?= HTTP_ROOT ?>lib/popper.min.js"></script>

        <link rel="stylesheet" href="<?= HTTP_ROOT ?>lib/bootstrap.min.css">
        <script src="<?= HTTP_ROOT ?>lib/bootstrap.min.js"></script>
        
<style>
  .wish {
    border-color:blue;
    box-shadow: 0 .5rem 1rem rgba(0,0,0,.15);
  }
  .wish.marked {
    border-color:lightgray;
    filter: blur(1px);
    box-shadow: none;
  }
  
  .wishlist.admin .wish {
    filter: blur(0);
    box-shadow: none;
    border-color:lightgray;
  }
  .wishlist.admin .wish.marked {
    color:gray;
  }
  .wishlist.admin .wish:hover {
    background-color: #eee;
  }
  
</style>
    </head>
    <body>
        <section class="container mt-4">
          <h1>Wunschliste von <?= htmlentities($USER->name) ?></h1>
        <?= get('body') ?>
        </section>
        <footer class="container my-5">
          <hr/>
          &copy; 2018 <a target="_blank" href="https://wapplications.net/">Stefan Beyer</a>
        </footer>
      <script>
          
          let submit_marked = function() {
              let $wish = $(this).closest('.wish');
              let id = $wish.data('id');
              let marked = $(this).prop('checked');
              $.post(<?= json_encode(listUrl('mark/')) ?>+id, {marked: marked}, function(d) {
                 if (d === 'ok') {
                     if (marked) {
                         $wish.addClass('marked');
                     } else {
                         $wish.removeClass('marked');
                     }
                 }
              });
          };
          
          
        </script>
      
    </body>
</html>