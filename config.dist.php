<?php
if (!defined('INTERN')) die();

$cfg = new stdClass();

/**
 * 'list'=>'ronny'
 * 'edit'=>'abc'
 * 
 * Liste:
 * example.com/list/ronny
 * Bearbeiten:
 * example.com/edit/abc
 * 
 */
$cfg->users = [];
$cfg->users[] = (object)['name'=>'Ronny', 'list'=>'ronny', 'edit'=>'abc'];

return $cfg;