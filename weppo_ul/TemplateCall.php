<?php

/**
 * The simplest way to create Output.
 * 
 *  1. There is a PHP file that will serve as a Template
 *  2. There is an Array of Data, that will be made available in that template.
 * 
 * It a Template calls other Templates, the $data will be stacked. If you request
 * a value, that is not present in the top of the stack, we go deeper in the stack to try and find it.
 * 
 * A value in this stacked Data can be...
 *  1. a Clusure that gets called if you request that value. No params will be set.
 *  2. An Object of a Class that implements iTemplateContentProvider. If you request that value, the 
 *     content_provider_get() method gets called with the requested name.
 * 
 *  - There is one global funtion template() to call a template and get the generated output.
 *    Along with the requested template filename, you pass a data array that will be available for that call
 *    and partly for subsequent template calls.
 *  - There are two global functions for a template to request data from the stack: get() and gete().
 *    The latter escapes the result using htmlenities().
 * 
 * That's all.
 * All the magic happens in the class TemplateCall that you will probably never use directly.
 * 
 */
namespace WUL {

    class TemplateCall {
        static $stack = [];
        static $baseDir = './';
        
        static function setDir($d) {
            if (substr($d, -1) !== '/') $d.='/';
            self::$baseDir = $d;
        }
        
        static function getDir() {
            return self::$baseDir;
        }
        
        static function pushData(&$data) {
            array_unshift(self::$stack, $data);
        }
        
        static function popData() {
            array_shift(self::$stack);
        }


        static function get($n) {
            if (!count(self::$stack)) return null;
            $stackIndex = 0;
            $stackSize = count(self::$stack);
            while (!isset(self::$stack[$stackIndex][$n]) && $stackIndex<=$stackSize-1) {
                $stackIndex++;
            }
            if ($stackIndex >= $stackSize) return null;
            $v = self::$stack[$stackIndex][$n];
            if (is_a($v, \Closure::class)) {
                return $v();
            }
            if (is_subclass_of($v, iTemplateContentProvider::class)) {
                return $v->content_provider_get($n);
            }
            return $v;
        }
        
        static function call($fn, &$data) {
            self::pushData($data);
            $fn = self::getDir() . $fn;
            if (!file_exists($fn)) {
                echo 'Template File <code>'.htmlentities($fn).'</code> not found.';
            } else {
                include $fn;
            }
            self::popData();
        }
    }
} // namespace WUL

namespace {

    function template($fn, $data = []) {
        ob_start();
        \WUL\TemplateCall::call($fn, $data);
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    function &get($n, $htmlentities = false) {
        if (!$htmlentities) {
            $v = \WUL\TemplateCall::get($n);
            return $v;
        }
        $v = htmlentities(\WUL\TemplateCall::get($n));
        return $v;
    }
    
    function &gete($n) {
        return get($n, true);
    }            
} // empty amespace