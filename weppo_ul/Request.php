<?php

namespace WUL;

//class ParameterNotPresentException extends \Exception {}
//class ParameterCastException extends \Exception {}

class Request {
    private $originalPath, $pathArray, $getData, $postData;
    
    public function __construct($path, &$get, &$post) {
        if (substr($path, 0, 1) === '/') $path = substr($path, 1);
        $this->originalPath = $path;
        $this->pathArray    = self::parsePath($path);
        $this->postData     = $post;
        $this->getData      = $get;
    }
    
    static function parsePath(string $path) {
        if (empty($path)) return [];
        $result = explode('/', $path);
        
        return $result;
    }
    
    function consumePath($count = 1) {
        for ($i=0; $i<$count; $i++) {
            $result = array_shift($this->pathArray);
        }
        return $result;
    }
    
    
    
    private function raw($name, &$source) {
        return $source[$name] ?? null;
    }
    
    
    function gRaw($name) {
        return $this->raw($name, $this->getData);
    }
    
    function pRaw($name) {
        return $this->raw($name, $this->postData);
    }
    
    
    function gHas($name) {
        return !is_null($this->gRaw($name));
    }
    
    function pHas($name) {
        return !is_null($this->pRaw($name));
    }
    
    protected function intCast($v) {
        if (is_null($v) || !is_numeric($v)) return null;
        return intval($v);
    }
    
    protected function floatCast($v) {
        if (is_null($v) || !is_numeric($v)) return null;
        return floatval($v);
    }
    
    protected function boolCast($v) {
        if (is_null($v)) return null;
        if ($v === 'true' || $v === 'TRUE' || $v === '1' || $v === 1) return true;
        if ($v === 'false' || $v === 'FALSE' || $v === '0' || $v === 0) return false;
        return null;
    }
    
    
    function gString($name, $default = "") {
        return $this->gRaw($name) ?? $default;
    }
    
    function pString($name, $default = "") {
        return $this->pRaw($name) ?? $default;
    }
    
    function gInt($name, $default = "") {
        return $this->intCast($this->gRaw($name)) ?? $default;
    }
    
    function pInt($name, $default = "") {
        return $this->intCast($this->pRaw($name)) ?? $default;
    }
    
    function gFloat($name, $default = "") {
        return $this->floatCast($this->gRaw($name)) ?? $default;
    }
    
    function pFloat($name, $default = "") {
        return $this->floatCast($this->pRaw($name)) ?? $default;
    }

    function gBool($name, $default = "") {
        return $this->boolCast($this->gRaw($name)) ?? $default;
    }
    
    function pBool($name, $default = "") {
        return $this->boolCast($this->pRaw($name)) ?? $default;
    }
    
    /**
     * Beispiel:
        $g = ['a'=>'123', 'b'=>'ertzui', 'c'=>'false', 'd'=>'true', 'e'=>'1'];
        $r = new WUL\Request('/', $g, $g);
        o($r->gExtract(['a', 'b', 'c', 'd', 'e', 'f']));
        o($r->gExtract(['a'=>'int', 'b'=>'string', 'c'=>'bool', 'd'=>'bool', 'e'=>'bool', 'f'=>'string', 'g'=>['string', '###']]));
     * 
     * @param array $fields
     * @return type
     */
    function gExtract(array $fields) {
        $isAssoc = !isset($fields[0]);
        $result = [];
        foreach ($fields as $k=>$v) {
            if ($isAssoc) {
                if (is_array($v)) {
                    list($type, $default) = $v;
                } else {
                    $type = $v;
                    $default = null;
                }
                switch ($type) {
                    case 'string': $value = $this->gString($k, $default); break;
                    case 'int': $value = $this->gInt($k, $default); break;
                    case 'float': $value = $this->gFloat($k, $default); break;
                    case 'bool': $value = $this->gBool($k, $default); break;
                    default:
                        $value = $this->gRaw($k);
                        if (is_null($value)) $value = $default;
                }
                $result[$k] = $value;
            } else {
                $result[$v] = $this->gRaw($v);
            }
        }
        return $result;
    }
    
    function pExtract(array $fields) {
        $isAssoc = !isset($fields[0]);
        $result = [];
        foreach ($fields as $k=>$v) {
            if ($isAssoc) {
                if (is_array($v)) {
                    list($type, $default) = $v;
                } else {
                    $type = $v;
                    $default = null;
                }
                switch ($type) {
                    case 'string': $value = $this->pString($k, $default); break;
                    case 'int': $value = $this->pInt($k, $default); break;
                    case 'float': $value = $this->pFloat($k, $default); break;
                    case 'bool': $value = $this->pBool($k, $default); break;
                    default:
                        $value = $this->pRaw($k);
                        if (is_null($value)) $value = $default;
                }
                $result[$k] = $value;
            } else {
                $result[$v] = $this->pRaw($v);
            }
        }
        return $result;
    }
    
    
}