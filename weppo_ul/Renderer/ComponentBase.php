<?php

namespace WUL\Renderer;

abstract class ComponentBase {
    var $contents;
    var $props;
    
    public function __construct(Props &$props, &$contents) {
        $this->contents = $contents;
        $this->props = $props;
    }
    
    abstract public function render();
    
    protected function prop($name, $default = null) {
        return $this->props->get($name, $default);
    }
}


