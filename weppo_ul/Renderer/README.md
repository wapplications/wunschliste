 

## Concept

The main thing in this renderer is a Component. A Component persists of three ingredient:

  1. a Component **Name**
  2. Properties (**Props**, they are like html attributes)
  3. an array of **Contents** (or children if you will)
 
The Component name is like a tag name in HTML. Indeed: If you use a HTML tag name
the resulting code will be excactly that html tag.
If the name contains some ":" (for ex. "c:Item") it gets interesting.
This will be treated as a call for a defined Component that will be rendered elsewhere.
 
Props are usually `Props` objects because they have some utility functions.
Internally they are assoc arrays.
Some functions need a Props object, some accept an array as well (they will get converted internally)
Generally prop values can be what ever you want them to be.
If a value is a `Closure` (callback function), that callback will be called, to retreive the value.

**Note: If a simple HTML Component is rendered (rendered into html code string for output by `renderComponent()`), the used Props must resolve to scalar values because they will become html attributes.**

The Contents part is a simple array of so called Component Descriptors.
A Component Descriptor can be

  - **Type 1** a string of html code, that will be rendered as is.
  - **Type 2** a Closure (callback function) that returns a string of html code.
  - **Type 3** an array that describes a call for a component in the form of
    `[comonent-name, props, contents]`

Example that renders to a standart html hyperlink: `['a', ['href'=>'https://example.com/'], ['click here']]`
will be rendered as `<a href="https://example.com/">click here</a>`.


## Rendering Process 
 
The rendering process will be as follows:

 1. A template file will be loaded (simple html file or php file)
    This process can be startet via `renderFile()` or if you already have the code
    in a string you would use `render()`.
 2. The loaded markup code gets tokenized into an array of Component Descriptors.
    See `tokenize()`.
    Simple HTML-Code will be represented as string of html code (content descriptor type 1)
    and not parsed into more detailed component descriptors of type 3.
    Elements, that contain a `:` in its name like `<c:Item ...></c:Item>` will
    be transformed into a component descriptor of type 3.
    Every HTML attribute will become a prop value in the components Props object.
    The HTML attributes can have a type prefix so that they will be converted to
    a specific type: `int:number="45"` will result in an integer value with prop name `number`,
    `date:today="2018-06-12"` will result in a DateTime object with prop name `today`...
    The content of such an element will be parsed the same way to create the array
    of contents for the component descriptor. For ex. if the element only contains
    simple Text or HTML, the contents array will contain only one element of type string.
 3. The result (array of component descriptors) will be passed to a function that calls
    the coresponding renderer for each component.
    Components with simple names will be rendered as simple HTML elements.
    For other components (name contains `:`), there are three types of renderers
    that we will be looking for:
    
    a. *File Component:* The component can be defined in an other template file.
        In this case the component name must have a specific form:
        `type::path:to:file`: Note that the file type (php, htm, html) must come first,
        followed by a double colon. The filename (relative to FILE_COMPONENTS_DIR)
        comes without extension (the type in front of the `::` will be used as file extension)
        and the path seperators must be replaced with colons.
        So if the name is `php::btn:BigButton`, we call for the file
        `FILE_COMPONENTS_DIR/btn/BigButton.php` and treat it like any file in the first place.
        
    b. *Function Component Renderer:* A Component can be rendered by a simple function with signiture `name(Props &$p, array &$contents)`. If the component name is `c:btn:BigButton` we will look
        for the function `\c\btn\BigButton()`, that is the function `BigButton()`
        in the namespace `\c\btn`. If it exists, we call that function by passing
        the props and contents. The returned value should be the rendered HTML code.
        
    c. *Component Renderer Class:* A Component can be rendered by a Component object. It has to extend `\WUL\Renderer\ComponentBase` and implement the `render()` method. If the name is `c:btn:BigButton` we will look
        for a class called `\c\btn\BigButton`, that is the class `BigButton`
        in the namespace `\c\btn`. An Object of that class will be created
        with the given props and contents and `render()` will be called. 
        The returned value should be the rendered HTML code.


**Note that you have to place Component functions and classes in a non empty namespace
in order to use them as a component. In this case the name will always contain at least one colon. Otherwise they will be handled as simple html elements.**

**Note that even if the tokenizer does not create a Component Descriptor for
simple html elements you can nevertheless render simple html elements by means of a
Component Descriptor.**




## Functions

The function `renderContents()` takes an array of Component Descriptors and renders them.

The function `renderComponent()` is called to render one component.

The function `renderAttributes()` renders Props to html attributes. You can pass a list of
attribute names to that function, if you don't want to render alle props as attributes. The prop value must resolve to a scalar value.
    
The function `tokenize()` parses a string of markup code into an array of Component Descriptors.

The function `render()` takes a string of markup code, tokenzies it and renders the result with `renderContents()`.

The function `renderFile()` wants a filename and loads markup code from that file (php gets executed first). You can optionally pass props and contents. That code gets eventually rendered by `render()`.

The function `globalProps()` provides a global Props object.


