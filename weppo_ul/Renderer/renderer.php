<?php
/**
 * Stefan Beyer
 * 
 * This is a more compicated way of creating Output.
 * It is based on a (generated or not) markup code that gets parsed for component calls 
 * and gets tranformed to the desired html output.
 * 
 * For more information see README.md
 * 
 * 
 */

define('WUL_RENDERER_ROOT', dirname(__FILE__));

require_once WUL_RENDERER_ROOT.'/RenderException.php';
require_once WUL_RENDERER_ROOT.'/Props.php';
require_once WUL_RENDERER_ROOT.'/ComponentBase.php';
require_once WUL_RENDERER_ROOT.'/FileComponent.php';
require_once WUL_RENDERER_ROOT.'/functions.php';
