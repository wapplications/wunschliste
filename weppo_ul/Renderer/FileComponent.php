<?php

namespace WUL\Renderer;

class FileComponent extends ComponentBase {
    var $filename;
    
    public function __construct($file, Props &$props, &$contents) {
        parent::__construct($props, $contents);
        $this->filename = $file;
    }

    public function render() {
        if (!file_exists($this->filename)) {
            return null;
        }
        
        if (substr($this->filename, -4) === '.php') {
            ob_start();
            include $this->filename;
            $code = ob_get_contents();
            ob_end_clean();
        } else {
            $code = file_get_contents($this->filename);
        }
        return render($code);
    }
}

