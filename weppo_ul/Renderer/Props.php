<?php

namespace WUL\Renderer;

class Props {
    private $data = [];
    
    public function __construct(&$data = []) {
        $this->data = $data;
    }
    
    public function select(array $selection): Props {
        $result = [];
        foreach ($this->getAll() as $k=>$v) {
            if (!in_array($k, $selection)) continue;
            $result[$k] = $v;
        }
        return new Props($result);
    }
    
    public function overwrite($props) {
        if (is_a($props, self::class)) {
            $this->overwrite($props->getAll());
            return;
        }
        if (!is_array($props)) return;
        foreach ($props as $k=>$v) {
            $this->set($k, $v);
        }
    }
    
    public function get($n, $default=null) {
        if (!array_key_exists($n, $this->data)) return $default;
        $v = $this->data[$n];
        if (is_a($v, \Closure::class)) {
            $v = $v();
        }
        return $v;
    }
    
    public function has($n) {
        return array_key_exists($n, $this->data);
    }
    
    public function &getAll() {
        return $this->data;
    }
    
    public function set($n, $v) {
        $this->data[$n] = $v;
    }
}

