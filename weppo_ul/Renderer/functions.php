<?php

namespace WUL\Renderer;

define('TOKENIZER_DEBUG', false);

if (!defined('FILE_COMPONENTS_DIR')) {
    define('FILE_COMPONENTS_DIR', 'templates/');
}

const SELF_CLOSING = [
    'br','hr','input','img','meta','area','base', 'col','command','embed','keygen','link','menuitem','param','source','track','wbr'
];

/**
 * Use FileComponent to render a Component from file
 * 
 * @param string $fn Filename
 * @param \WUL\Renderer\Props | array $props
 * @param array $contents
 * @return string
 * @throws \WUL\Renderer\RenderException
 */
function renderFile($fn, $props=[], $contents=[]) {
    if (!file_exists($fn)) {
        throw new RenderException('File not Found '.$fn);
    }
    
    if (is_array($props)) {
        $props = new Props($props);
    }
    
    $fileComponent = new FileComponent($fn, $props, $contents);
    return $fileComponent->render();
}

/**
 * Parses the sode to an array of Component Descriptors.
 * For details see comment in README.md
 * 
 * @param string $code
 * @return array Component Descriptors
 */
function render($code) {
    $tokens = tokenize($code);
    if (TOKENIZER_DEBUG) {
        echo 'TOKENS:',PHP_EOL;
        print_r($tokens);
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
    }
    return renderContents($tokens);
}


/**
 * Parses html Code into Component Descriptors;
 * 
 * @param string $code
 * @return array
 */
function tokenize($code) {
    if (empty($code)) return [];
    
    $tokens = [];
    $length = strlen($code);
    $offset = 0;
    
    $componentStart = null;
    $componentInnerStart = null;
    $componentInnerEnd = null;
    $componentEnd = null;
    $componentLevel = 0;
    $componentName = null;
    $attributes = null;
    
    $_c = 0;
    
    while ($offset < $length /* && $_c<10*/) {
        $_c++;
        
        
        $patterns = [
            '#<([0-9a-zA-Z_\\.]+:[0-9a-zA-Z_:\\.]+)#',
            '#</[0-9a-zA-Z_\\.]+:[0-9a-zA-Z_:\\.]+>#',
            '#<!\\[CDATA\\[#',
        ];
        $matches = [];
        
        $minPos = null;
        $pIndex = null;
        foreach ($patterns as $k=>$p) {
            if (preg_match($p, $code, $_matches, PREG_OFFSET_CAPTURE, $offset)) {
                $matches[] = $_matches;
                if (is_null($minPos) || $_matches[0][1] < $minPos) {
                    $minPos = $_matches[0][1];
                    $pIndex = $k;
                }
            } else {
                $matches[] = null;
            }
        }
        if (TOKENIZER_DEBUG) {
            print_r($matches);
            echo '[$pIndex=',$pIndex,']',PHP_EOL;
        }
        if (is_null($pIndex)) {
            if (TOKENIZER_DEBUG) {
                echo 'Nix zu finden', PHP_EOL;
            }
            break;
        }
        
        
        # CDATA start
        if ($pIndex === 2) {
            $matches = $matches[2];
            $offset = $matches[0][1] + strlen($matches[0][0]);
            $cdata_start = $offset;
            if (preg_match('#\\]\\]>#', $code, $end_matches, PREG_OFFSET_CAPTURE, $offset)) {
                $cdata_end = $end_matches[0][1];
                $offset = $end_matches[0][1] + strlen($end_matches[0][0]);
                if (is_null($componentStart) && $componentLevel === 0) {
                    $componentStart = $matches[0][1];
                    $componentEnd = $end_matches[0][1] + strlen($end_matches[0][0]);
                    $componentInnerStart = $cdata_start;
                    $componentInnerEnd = $cdata_end;
                    $componentName = 'CDATA';
                    $attributes = '';
                }
            } else {
                if (TOKENIZER_DEBUG) {
                    echo 'Ende von CDATA nicht gefunden.';
                }
                break;
            }
            //continue;
        }
        
        
        # Closing Tag
        if ($pIndex === 1) {
            $pIndex = null;
            $matches = $matches[1];
            #print_r($matches);
            $offset = $matches[0][1] + strlen($matches[0][0]);
            if (is_null($componentEnd) && $componentLevel===0) {
                if (TOKENIZER_DEBUG) {
                    echo 'Closing Tag', PHP_EOL;
                }
                $componentEnd = $matches[0][1] + strlen($matches[0][0]);
                $componentInnerEnd = $matches[0][1];
            } else {
                $componentLevel--;
            }
        }
        
        
        
        # Opening Tag
        if ($pIndex === 0) {
            $pIndex = null;
            $matches = $matches[0];
            //print_r($matches);
            
            // start des matches + tagname-match
            $offset = $matches[0][1] + strlen($matches[0][0]);
            
            // Ende suchen
            if (preg_match('#>#', $code, $end_matches, PREG_OFFSET_CAPTURE, $offset)) {
                
                $attributesStart = $offset;
                $startTagEnd = $end_matches[0][1];
                $zeichendavor = substr($code, $startTagEnd-1, 1);
                $zeichendanach = substr($code, $startTagEnd+1, 1);
                $offset = $startTagEnd+1;
            } else {
                if (TOKENIZER_DEBUG) {
                    echo 'Ende von starttag nicht gefunden.';
                }
                break;
            }
            
            

            if (is_null($componentStart) && $componentLevel===0) {
                $componentStart = $matches[0][1];
                $componentName = $matches[1][0];
                if (TOKENIZER_DEBUG) {
                    print_r(['StartTagStrt'=>$componentName, 'start'=>$componentStart, 'neuoffset'=>$offset, 'rest'=>substr($code, $offset)]);
                }
                
                $componentInnerStart = $startTagEnd+1;
                $attributesEnd = $startTagEnd;
                if ($zeichendavor == '/') {
                    if (TOKENIZER_DEBUG) {
                        echo 'Self-Closing Tag', PHP_EOL;
                    }
                    $componentEnd = $startTagEnd+1;
                    $componentInnerEnd = $componentInnerStart;
                    $attributesEnd--;
                }
                $attributes = substr($code, $attributesStart, $attributesEnd-$attributesStart);
                $offset = $componentInnerStart;
                
            } else if ($zeichendavor != '/') {
                $componentLevel++;
            }
        }
        
        if (TOKENIZER_DEBUG) {
            echo 'S:',$componentStart, '-E:', $componentEnd,'|';
        }
        if (!is_null($componentStart) && !is_null($componentEnd)) {
            $tagInner = substr($code, $componentInnerStart, $componentInnerEnd-$componentInnerStart);
            if (TOKENIZER_DEBUG) {
                print_r([
                    'EndTag'=>$componentEnd,
                    'cname'=>$componentName,
                    'cinnerstart'=>$componentInnerStart,
                    'cinnerend'=>$componentInnerEnd,
                    '$tagInner'=>$tagInner,
                    '$attributes'=>$attributes,
                    '$offset'=>$offset
                ]);
            }
            
            if ($componentName !== 'CDATA') {
                $before = substr($code, 0, $componentStart);
                if (!empty($before)) {
                    $tokens[] = $before;
                }
                
                $tokens[] = [
                    $componentName,
                    parseAttributes($attributes),
                    tokenize($tagInner)
                ];
                
                $code = substr($code, $componentEnd);
            } else {
                # Sonderbehandlung für CDATA
                
                # die ersten alphanumerischen zeichen können einen datentyp darstellen
                if (preg_match('#^([a-zA-Z0-9]+):#', $tagInner, $type_matches, PREG_OFFSET_CAPTURE)) {
                    $type = strtolower($type_matches[1][0]);
                    $tagInner = substr($tagInner, strlen($type_matches[0][0]));
                } else $type = 'cdata';
                unset($type_matches);
                $tagInner = trim($tagInner);
                $attributes = ['type'=>'cdata', 'cdata'=>$tagInner];
                if ($type === 'json') {
                    $attributes['type'] = 'json';
                    $attributes['json'] = json_decode($tagInner);
                }
                $tokens[] = [
                    'CDATA',
                    $attributes,
                    []
                ];
                $code = '';
            }
            
            $length = strlen($code);
            $offset = 0;
            
            $componentName = null;
            $componentStart = null;
            $componentEnd = null;
            $attributes = null;
        }
        
        
        if (TOKENIZER_DEBUG) {
            echo '-----------------------------', PHP_EOL;
        }
    }
    
    if (!empty($code)) {
        $tokens[] = $code;
    }
    
    
    
    return $tokens;
}

/**
 * Parses html attributes into Props
 * 
 * @param string $strAttributes
 * @return \WUL\Renderer\Props
 */
function parseAttributes($strAttributes) {
    $strAttributes = trim($strAttributes);
    $result = [];
    if (empty($strAttributes)) return $result;
    
    $pattern = '#([a-zA-Z:_\\-]+)=("([^"]*)"|\'([^\']*)\')#';
    if (preg_match_all($pattern, $strAttributes, $matches, PREG_SET_ORDER)) {
        foreach ($matches as &$m) {
            $value = !isset($m[4]) ? $m[3] : $m[4];
            
            $attrNameParts = explode(':', $m[1]);
            if (count($attrNameParts) == 2) {
                list($varType,$varName) = $attrNameParts;
            } else {
                $varType = 'string';
                $varName = $attrNameParts[count($attrNameParts)-1];
            }
            
            switch ($varType) {
                case 'int': $value = intval($value); break;
                case 'float': $value = floatval($value); break;
                case 'json': $value = json_decode($value); break;
                case 'date': 
                    try {
                        $value = new \DateTime($value);
                    } catch (\Exception $e) {
                        $value = null;
                    }
                    break;
            }
            
            $result[$varName] = $value;
        }
    }
    $result2 = new Props($result);
    return $result2;
}

/**
 * Renders one component.
 * 
 * @param string $name
 * @param \WUL\Renderer\Props $props
 * @param array $contents
 * @return string
 * @throws RenderException
 */
function renderComponent($name, $props, $contents) {
    if (is_array($props)) {
        $props = new Props($props);
    }
    
    if (strpos($name, ':') !== false) {
        try {
            $html = null;
            if (strpos($name, '::') !== false) {
                // file component
                list($type, $filename) = explode('::', $name);
                if (!$type || !$filename) throw new RenderException('failed to analyse component name');
                if (!in_array($type, ['php','html','htm'])) throw new RenderException('component file type not allowed');
                $filename = FILE_COMPONENTS_DIR . implode('/', explode(':', $filename)).'.'.$type;
                $html = renderFile($filename, $props, $contents);
            } else {
                // other renderers
                $nameParts = explode(':',$name);
                $rendererName = '\\'.implode('\\', $nameParts);
                
                // if it's a function...
                if (function_exists($rendererName)) {
                    $html = $rendererName($props, $contents);
                }
                // if it's a Class
                else if (is_subclass_of($rendererName, ComponentBase::class)) {
                    $component = new $rendererName($props, $contents);
                    $html = $component->render();
                }
            }
            if (is_null($html)) {
                throw new RenderException('no renderer found');
            }
        } catch (RenderException $e) {
            $html = '['.$name.': '.$e->getMessage().']';
        }
        return $html. PHP_EOL;
    } else if ($name === 'CDATA') {
        $type = $props->get('type');
        if (!$type) $type = '';
        $element = '<![CDATA['.($type ? $type.':':'').PHP_EOL;
        if ($type === 'json') {
            $element .= json_encode($props->get('json'), JSON_PRETTY_PRINT);
        } else {
            $element .= $props->get('cdata');
        }
        $element .= PHP_EOL.']]>';
        return $element;
    } else { // simple html element
        $element = '<'.$name;
        $element .= renderAttributes($props);
        if (in_array($name, SELF_CLOSING)) {
            $element .= ' />'.PHP_EOL;
        } else {
            $element .= '>'. PHP_EOL;
            $element .= renderContents($contents);
            $element .= '</'.$name.'>'. PHP_EOL;
        }
        return $element;
    }
}

/**
 * Takes an array of Component Descriptors and renders them.
 * 
 * @param array $contents
 * @return string
 */
function renderContents(&$contents) {
    $html = '';
    foreach ($contents as &$c) {
        if (is_string($c)) {
            // html codieren oder nicht??
            $html .= $c . PHP_EOL;
        } else if (is_a($c, \Closure::class)) {
            $html .= $c(); // TODO darf das Ergebnis ein Component descriptor sein??
        } else if (is_array($c)) {
            if (count($c)===3) {
                list($_name, $_props, $_contents) = $c;
                $html .= renderComponent($_name, $_props, $_contents);
            } else {
                // ???
            }
        }
    }
    return $html;
}

/**
 * Renders Props to html attributes. You can pass a list of
 * attribute names to that function, if you don't want to render alle props as attributes.
 * The prop value must resolve to a scalar value.
 * 
 * @param \WUL\Renderer\Props $props
 * @param array $selection selection of props that will be rendered. all if null.
 * @return string
 */
function renderAttributes(Props &$props, array $selection = null) {
    $attr = '';
    // TODO, wenn $v callable ist (closure)
    $attr_array = &$props->getAll();
    foreach ($attr_array as $k=>$v) {
        if (is_null($v)) continue;
        if ($selection !== null && !in_array($k, $selection)) continue;
        
        // TODO $v vorbereiten!
        if (!is_scalar($v)) {
            if (is_a($v, \Closure::class)) {
                $v = $v();
            }
        }
        
        # nicht scalare werte überspringen
        if (is_null($v) || !is_scalar($v)) continue;
        
        /*if (is_bool($v)) $v = $v ? '1' : '0';
        else $v = ''.$v;*/
        if (is_bool($v)) {
            if ($v) {
                $attr .= ' '.$k;
            }
            continue;
        }
        
        #$v = str_replace('"', '&quot;', $v);
        $v = htmlentities($v);
        
        $attr .= ' '.$k.'="'.$v.'"';
    }
    return $attr;
}


function text($t) {
    return htmlentities($t);
}



function &globalProps() {
    static $props_array = [];
    static $props = null;
    if (is_null($props)) {
        $props = new Props($props_array);
    }
    return $props;
}


function extractJSON(&$contents) {
    if (!count($contents)) return null;
    list($name, $a, $ccc) = $contents[0];
    if ($name !== 'CDATA') return null;
    if (!isset($a['json'])) return null;
    return $a['json'];
}

function extractCDATA(&$contents) {
    if (!count($contents)) return null;
    list($name, $a, $ccc) = $contents[0];
    if ($name !== 'CDATA') return null;
    if (!isset($a['cdata'])) return null;
    return $a['cdata'];
}


function renderJSONComponent(&$data) {
    return \WUL\Renderer\renderComponent('CDATA', ['type'=>'json','json'=>$data], []);
}


