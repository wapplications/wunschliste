<?php

namespace WUL;

abstract class DataObject implements \JsonSerializable {
    
    static function get_vars() {
        throw new \Exception('Class "' . get_called_class().'" must redefine "static function get_vars()"');
    }
    
    static function get_var_names() {
        return array_keys(static::get_vars());
    }
    
    static function get_id_var() {
        if (isset(static::get_vars()[0])) {
            throw new \Exception('Class "' . get_called_class().'" uses old method for the array returned by "get_vars()"');
        }
        $v = static::get_vars();
        reset($v);
        return key($v);
    }
    
    
    public function __construct($data = null, $save_to_cache = true) {
        if ($data === null) return;
        $this->fromObject($data);
        if ($save_to_cache) {
            if (!property_exists($this, static::get_id_var()) || !$this->{static::get_id_var()}) {
                $this->new_id();
            }
            $key = $this->{static::get_id_var()};
            static::$cache[$key] = &$this;
        }
    }
    
    
    public static function &get_item($key) {
        if (!array_key_exists($key, static::$cache)) {
            throw new \Exception('item not found');
        }
        return static::$cache[$key];
    }

    public static function &get_cache() {
        return static::$cache;
    }
    
    
    function delete() {
        $id = $this->{static::get_id_var()};
        if (array_key_exists($id, static::$cache)) {
            unset(static::$cache[$id]);
            static::save();
        }
    }
    
    function fromObject($obj) {
        $vars = static::get_var_names();
        foreach ($vars as $v) {
            if (!property_exists($obj, $v)) {
                $this->{$v} = null;
                continue;
            }
            $this->{$v} = $obj->{$v};
        }
    }
    
    public function jsonSerialize() {
        $vars = static::get_var_names();
        $object = new \stdClass();
        foreach ($vars as $v) {
            $object->{$v} = $this->{$v};
        }
        return $object;
    }
    
    
    static function load() {
        // TODO Lock
        $json_filename = static::$filename;

        if (file_exists($json_filename)) {
            $data = json_decode(file_get_contents($json_filename));
            if ($data===null || !is_array($data)) {
                die('DataObject::load(): Fehler beim Laden von '.$json_filename);
            }
            foreach ($data as &$d) {
                $class = static::class;
                new $class($d);
            }
        }
    }
    
    
    static function save() {
        // TODO UNLock
        $json_filename = static::$filename;

        if (file_exists($json_filename.'.last')) {
            rename($json_filename.'.last', $json_filename.'.beforelast');
        }
        copy($json_filename, $json_filename.'.last');

        $data = [];
        foreach (static::$cache as &$aim) {
            $data[] = $aim;
        }
        $data = json_encode($data, JSON_PRETTY_PRINT);
        // TODO unüberschreibbare kopie des letzten backups erstellen ? verhindern dass festplatte vollgeschrieben wird jedesmal
        if ($data === false) {
            die('DataObject::save(): Fehler beim Speichern. Seite nicht neu laden! Sichern Sie zunächst die Backup-Dateien "'.$json_filename.'.last" und "'. $json_filename.'.beforelast"');
        }
        file_put_contents($json_filename, $data);
    }
    
    public function new_id() {
        $idVar = static::get_id_var();
        $maxId = 0;
        $cache =& static::get_cache();
        foreach ($cache as &$item) {
            if ($item->{$idVar} > $maxId) {
                $maxId = $item->{$idVar};
            }
        }
        $this->{$idVar} = $maxId+1;
    }
    
    
}