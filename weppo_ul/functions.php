<?php

namespace WUL;

function redirect($url=null) {
    if (is_null($url)) {
        $url = $_SERVER['REQUEST_URI'];
    }
    header('Location: ' . $url, true, 303);
    exit();
}
