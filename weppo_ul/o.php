<?php


function oo($exp, $maxlevel=2, $html=true) {
    $result = '';
    if ($html) $result = '<pre>';
    $result .= _o($exp, 0, $maxlevel, $html);
    if ($html) $result .= '</pre>';
    return $result;
}


function o($exp, $maxlevel=2, $html=true) {
    echo oo($exp,$maxlevel, $html);
}

function co($exp, $maxlevel=2) {
    console(PHP_EOL.oo($exp,$maxlevel, false));
}


function _o($exp, $level, $maxlevel, $html=true) {
    ob_start();
    //echo str_repeat("  ", $level);
    if (is_array($exp)) {
        echo '[', PHP_EOL;
    } else if (is_object($exp)) {
        if ($html) echo '<strong>';
        echo get_class($exp);
        if ($html) echo '</strong>';
        echo ' {', PHP_EOL;
    } else if (is_null($exp)) {
        if ($html) echo '<em>';
        echo 'NULL';
        if ($html) echo '</em>';
        echo PHP_EOL;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    } else if (is_string($exp)) {
        $v = $exp;
        if ($html) $v = htmlspecialchars($exp, ENT_COMPAT|ENT_SUBSTITUTE, "UTF-8");
        echo '"' . $v . '"', PHP_EOL;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    } else {
        echo $exp, PHP_EOL;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    foreach ($exp as $k=>&$a ) {
        echo str_repeat("  ", $level+1);
        if ($html) echo '<span style="color:blue;">';
        echo htmlspecialchars($k);
        if ($html) echo '</span>';
        echo ': ';
        if (is_array($a)) {
            if ($level <= $maxlevel) {
                if (count($a) == 0) {
                    if ($html) echo '[<span style="font-style:italic;">leer</span>]', PHP_EOL;
                    else echo '[leer]', PHP_EOL;
                } else if (count($a) <= 5) {
                    $ok = true;
                    $l = 0;
                    foreach ($a as $k=>&$s) {
                        if (is_array($s) || is_object($s) || is_string($k)) {
                            $ok = false;
                            break;
                        }
                        $l += strlen($s);
                    }
                    if ($ok && $l < 120) {
                        echo '[';
                        if ($html) $_a = array_map(function($__a){return '"'.htmlspecialchars($__a).'"';}, $a);
                        else $_a = array_map(function($__a){return '"'.$__a.'"';}, $a);
                        echo implode(', ', $_a);
                        echo ']', PHP_EOL;
                    } else {
                        echo _o($a, $level+1, $maxlevel, $html);
                    }
                } else {
                    echo _o($a, $level+1, $maxlevel, $html);
                }
            } else {
                if ($html) echo '<span style="font-style:italic;">', '[...]', '</span>', PHP_EOL;
                else echo '[...]', PHP_EOL;
            }
        } else if (is_object($a)) {
            if ($level <= $maxlevel) {
                echo _o($a, $level+1, $maxlevel, $html);
            } else {
                if ($html) echo '<span style="font-style:italic;">','Object...', '</span>', PHP_EOL;
                else echo 'Object...', PHP_EOL;
            }
        } else if (is_null($a)) {
            if ($html) echo '<em>NULL</em>', PHP_EOL;
            else echo 'NULL', PHP_EOL;
        } else if (is_string($a)) {
            if ($html) echo '"', htmlspecialchars($a), '"', PHP_EOL;
            else echo '"', $a, '"', PHP_EOL;
        } else {
            echo $a, PHP_EOL;
        }
    }

    echo str_repeat("  ", $level);

    if (is_array($exp)) {
        echo ']', PHP_EOL;
    } else if (is_object($exp)) {
        echo '}', PHP_EOL;
    }

    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}


function console($s) {
  $date = date('Y-m-d H:i:s');
  if (!is_scalar($s)) {
    co($s);
    return;
  }
  file_put_contents('php://stdout', '['.$date.'] '.$s.PHP_EOL);
}
