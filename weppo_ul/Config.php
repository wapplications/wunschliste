<?php

namespace WUL;

class Config {
    static $config = null;
    public static function &getConfig() {
        return self::$config;
    }
    public static function setConfig($c) {
        self::$config = $c;
    }
}

