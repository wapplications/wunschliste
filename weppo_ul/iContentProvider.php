<?php

namespace WUL;

interface iContentProvider {
    public function content_provider_get($n);
}